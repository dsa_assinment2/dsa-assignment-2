import ballerina/io;
import ballerina/grpc as grpc;

ReposityOfFunctionsClient ep = check new ("http://localhost:9090");
public function main() {

    io:print();

    AddFnReq addFnReq = {

        fun: {

            name: "second assignment",
            versionNum: 1,
            ang: "Balleina",
            fnDef: "print('second assignment')",
            keywords: ["student"],
            devName: "sam",
            devEmail: "sam@sam"

        }

    };

    AddFnRes|grpc:Error addFnsRes = ep->add_new_fn(addFnReq);

    if addFnsRes is error {

        io:println("Error adding new fun: ", addFnsRes.message());

    } else {

        io:println("completed: ", addFnsRes.message);

    } //heree we've two completed and error it will move based on the result

    AddFnsReq[] addFnsReq = [

        {

        fun: {

            devEmail: "sam@sam",
            keywords: ["student", "sam"],
            versionNum: 1,
            name: "second assignment1",
            lang: "ballerina1",
            devName: "sam1",
            fnDef: "test1"

        }

    },

        {

        fun: {
devEmail: "sam@sam",
            keywords: ["student", "sam"],
            versionNum: 1,
            name: "second assignment1",
            lang: "ballerina1",
            devName: "sam1",
            fnDef: "test1"
        }

    },

        {

        fun: {

            devEmail: "sam@sam",
            keywords: ["student", "sam"],
            versionNum: 1,
            name: "second assignment2",
            lang: "ballerina2",
            devName: "sam2",
            fnDef: "test2"

        }

  }       //here we are testing the cases implemented above

    ];

    Add_fnsStreamingClient|grpc:Error addFnsStream = ep->add_fns();

    if addFnsStream is error {
        io:println("error added funs: ", addFnsStream.message());

    } else {

        foreach AddFnsReq aFReq in addFnsReq {

            grpc:Error? err = addFnsStream->sendAddFnsReq(aFReq);

            if err is error {

                io:println("Failed to send addFns request");

            }

        }

        grpc:Error? err = addFnsStream->complete();

        if err is error {
            io:println("Failed to send complete message");
            } else {
                AddFnsRes|grpc:Error? fnsRes = addFnsStream->receiveAddFnsRes();
                
                if fnsRes is error {
                    io:println("Failed to retrieve addFnsRes: ", fnsRes.message());

           } else {
               if fnsRes is AddFnsRes {

                    foreach string msg in fnsRes.funcNames {
                        io:println(msg);

                    }

                }

            }

        }

    } 

    ShowFnReq showFnReq = {

        funcName: "second assignment",
        versionNum: 1

    };

    ShowFnRes|grpc:Error showFnRes = ep->show_fn(showFnReq);

    if showFnRes is error  {

        io:println("Error retrieving fun: ", showFnRes.message());

    } else {

        io:println("completed: ", showFnRes);

    }

    ShowAllFnsReq showAllFnsReq = {

        funcName: "second assignment"

    };

    stream<ShowAllFnsRes, grpc:Error?>|grpc:Error showAllFnsStream = ep->show_all_fns(showAllFnsReq);

    if showAllFnsStream is error {

        io:println("Failed to show all funs: ", showAllFnsStream.message());

    } else {

        error? e = showAllFnsStream.forEach(function(ShowAllFnsRes res) {

            io:println("Successfully retrieved fun: ", res.fun.name);

        });

    }

    ShowAllWithCritReq[] showAllWithCritReqs = [{

        keywords: ["student", "sam"],

        lang: "ballerina"

    }];

    Show_all_with_criteriaStreamingClient|grpc:Error showAllWithCritStream = ep->show_all_with_criteria();

    if showAllWithCritStream is error {

        io:println("error occured setting up showAllWithCrit stream: ", showAllWithCritStream.message());

    } else {

        foreach ShowAllWithCritReq showAllWithCritReq in showAllWithCritReqs {

            error? err = showAllWithCritStream->sendShowAllWithCritReq(showAllWithCritReq);

            if err is error {

                io:println("failed to send showAllWithCriteria req: ", err.message());

            }

        }

        error? err = showAllWithCritStream->complete();

        if err is error {

            io:println("failed to send showAllWithCriteria complete message: ", err.message());

        }

        ShowAllWithCritRes|grpc:Error? showAllWithCritRes = showAllWithCritStream->receiveShowAllWithCritRes();

        while true {

            if showAllWithCritRes is error {

                io:println("failed to send showAllWithCriteria complete message: ", showAllWithCritRes.message());

                break;

            } else {

                if showAllWithCritRes is () {

                    break;

              } else {

                    io:println("showAllWithCriteria response: ", showAllWithCritRes.fns);

                    showAllWithCritRes = showAllWithCritStream->receiveShowAllWithCritRes();

                }

            }

        }

    }

    DeleteFnReq delFnReq = {

        funcName: "Assignment2",

        versionNum: 1

    };

    DeleteFnRes|grpc:Error delRes = ep->delete_fn(delFnReq);

    if delRes is error {

        io:println("Error deleting fn: ", delRes.message());

    } else {

       io:println("completed ", delRes.message);

    }

}